cp -s --remove-destination ~/dotfiles/dotfiles/vimrc ~/.config/nvim/init.vim
cp -s --remove-destination ~/dotfiles/dotfiles/vimrc ~/.vimrc
cp -s --remove-destination ~/dotfiles/dotfiles/tmux ~/.tmux.conf
cp -s --remove-destination ~/dotfiles/dotfiles/zshrc ~/.zshrc
cp -s --remove-destination ~/dotfiles/dotfiles/spacemacs ~/.spacemacs
cp -s --remove-destination ~/dotfiles/dotfiles/profile ~/.profile
cp -s --remove-destination ~/dotfiles/dotfiles/zshenv ~/.zshenv

# install pyenv
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv

mkdir -p ~/.tmux/plugins/
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# install plugins for vim
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

mkdir -p ~/.vim/plugged/

# install fzf
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install --all

# install tmux plugin manager
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
